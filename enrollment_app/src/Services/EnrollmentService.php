<?php
namespace App\Services;

use App\Entity\EnrollmentData;
use GuzzleHttp\Client;

class EnrollmentService
{
    private $enrollmentData;
    private $adminUsername = 'enrollment_app';
    private $adminPassword = 'Cug46fNhD9f2vKye';
    private $adminGroup = 2;
    private $cladeToken = '5rerEGSdv51bvd3V5v+ds/sv++V1Sv15SdvSDv1SDDVSv15d8vv58v13V20vSv51V48SDCV58dcd8';
    private $adminToken;
    private $newUserGroup = "1000";
    private $salt = 'salt';
    private $defaultPassword = 'password';
    private $storeUserUrl = 'https://d-side-server.data-warehouse.zone/v1/users';
    private $newUserLogin;
    private $newUserToken;
    private $studyArmId = 1;
    private $testSubject = false;
    private $studyId = 1621;

    public function __construct(EnrollmentData $enrollmentData)
    {
        $this->enrollmentData = $enrollmentData;
        $this->newUserLogin = $this->buildNewUserLogin($enrollmentData->getSurname());
    }

    public function storeData() : void
    {
        $this->loginAsSystemUser();
        $this->createNewUserAccount();
        $this->loginAsNewUser();
        $this->storeSubjectData();
    }

    private function loginAsSystemUser() : void
    {
        $client = new Client();
        $response = $client->post($this->buildLoginUrl($this->adminUsername), [
            'headers' => [
                'Accept' => 'application/json',
                'Clade-Application' => $this->cladeToken
            ],
            'json' => [
                'password' => base64_encode($this->adminPassword),
            ]
        ]);
        $jsonResponse = json_decode($response->getBody()->getContents(), true);
        $this->adminToken = $jsonResponse['data']['token'];
    }

    private function createNewUserAccount() : void
    {
        $client = new Client();
        $response = $client->post($this->storeUserUrl, [
            'headers' => [
                'Accept' => 'application/json',
                'Clade-Application' => $this->cladeToken,
                'Clade-Token' => $this->adminToken,
                'Clade-Group' => $this->adminGroup,
            ],
            'json' => [
                'userData' => json_encode($this->buildUserData()),
                'assignToGroups' => "[$this->newUserGroup]",
            ]
        ]);
        $jsonResponse = json_decode($response->getBody()->getContents(), true);
    }

    private function loginAsNewUser() : void
    {
        $client = new Client();
        $response = $client->post($this->buildLoginUrl($this->newUserLogin), [
            'headers' => [
                'Accept' => 'application/json',
                'Clade-Application' => $this->cladeToken
            ],
            'json' => [
                'password' => base64_encode($this->defaultPassword),
            ]
        ]);
        $jsonResponse = json_decode($response->getBody()->getContents(), true);
        $this->newUserToken = $jsonResponse['data']['token'];
    }

    private function storeSubjectData() : void
    {
        $client = new Client();
        $response = $client->post("https://d-side-server.data-warehouse.zone/v1/studies/$this->studyId/subjects", [
            'headers' => [
                'Accept' => 'application/json',
                'Clade-Application' => $this->cladeToken,
                'Clade-Token' => $this->newUserToken,
                'Clade-Group' => $this->newUserGroup,
            ],
            'json' => [
                'subjectData' => json_encode($this->buildSubjectData()),
                'formData' => json_encode($this->buildFormData()),
            ]
        ]);
        $jsonResponse = json_decode($response->getBody()->getContents(), true);
    }

    private function buildLoginUrl(string $username) : string
    {
        return "https://d-side-server.data-warehouse.zone/v1/users/$username/passwords";
    }

    private function buildNewUserLogin(string $surname) : string
    {
        return $surname . time();
    }

    private function buildUserData() : array
    {
        return [
            'name' => $this->enrollmentData->getFirstName(),
            'login' => $this->newUserLogin,
            'email' => $this->enrollmentData->getEmail(),
            'salt' => $this->salt,
            'password' => $this->defaultPassword
        ];
    }

    private function buildSubjectData() : array
    {
        return [
            'studyArmId' => $this->studyArmId,
            'testSubject' => $this->testSubject,
        ];
    }

    private function buildFormData() : array
    {
        return [
            'Q243' => $this->enrollmentData->getFirstName(),
            'Q244' => $this->enrollmentData->getSurname(),
            'Q245' => $this->enrollmentData->getPhoneNumber(),
            'Q246' => $this->enrollmentData->getEmail(),
            'Q247' => $this->enrollmentData->getInformedConsent(),
        ];
    }
}