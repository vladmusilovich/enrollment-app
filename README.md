## Enrollment App

### 1. Install [Docker](https://docs.docker.com/get-docker/)

### 2. Clone the repository
````
  git clone https://vladmusilovich@bitbucket.org/vladmusilovich/enrollment-app.git
````

### 2. Run docker-compose file
````
sudo docker-compose up -d
````
### 3. Setting Up Project Dependencies

1. Create `.env.local` file and specify `DATABASE_URL`

2. Open enrollment app container

````
sudo docker exec -ti clade_is_enrollment_app_enrollment_app_1 bash
````

3. Install dependencies

````
cd enrollment_app
````

````
composer install
````

4. Run migrations

````
php bin/console doctrine:migrations:migrate
````

5. Open http://localhost:8000


