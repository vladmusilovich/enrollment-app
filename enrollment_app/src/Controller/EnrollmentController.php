<?php

namespace App\Controller;

use App\Entity\EnrollmentData;
use App\Form\EnrollmentType;
use App\Services\EnrollmentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EnrollmentController extends AbstractController
{

    public function index(Request $request): Response
    {
        $enrollmentData = new EnrollmentData();
        $form = $this->createForm(EnrollmentType::class, $enrollmentData);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $enrollmentData = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($enrollmentData);
            $entityManager->flush();
            $enrollmentService = new EnrollmentService($enrollmentData);
            $enrollmentService->storeData();
            $this->addFlash('success', 'Your enrollment is submitted!');
        }

        return $this->render('enrollment/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
